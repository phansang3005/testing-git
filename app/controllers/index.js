import MultipleChoice from '../models/multipleChoice.js'
import FillInBlank from '../models/fillInBlank.js'
const questionList = [];

// Lấy data từ DB
// khai báo function kiểu es6 không làm thay đổi ngữ cảnh con trỏ this. nếu có this nó sẽ trỏ tới window còn nếu cần đổi ngữ cảnh xài function kiểu cũ
const getData = () => {
  axios({
    method: "GET",
    url: "../../DeThiTracNghiem.json"
  })
    .then(res => {
      
      
      mapDataToObject(res.data);
      // map()
      var contentArr = questionList.map((currentQuestion, i) =>{
        return currentQuestion;
      })
      console.log(contentArr);
      
      renderQuestion();
    })
    .catch(err => {
      console.log(err);
    }); // promise khái niệm của es6 dùng để sử dụng khi sever ko gửi về có 3 trạng thái pending(đang đợi, chưa trả db về), resolve(done), reject(fail)
};
const mapDataToObject = data => {
  for (let question of data) {
    
    // destructuring : bóc tách phần tử thứ i trong mảng data và lưu vào biến question để xử lý dữ liệu điều kiện
    const { questionType, _id, content, answers } = question;
    // => const questionType = question.questionType
    let newQuestion = {};
    if (questionType === 1) {
      newQuestion = new MultipleChoice(questionType, _id, content, answers);
    } else {
      newQuestion = new FillInBlank(questionType, _id, content, answers);
    }
    questionList.push(newQuestion);
  }
};

const renderQuestion = () => {
    let content = '';
    for(let i in questionList){
        content += questionList[i].render(i*1 + 1);
    }
    document.getElementById('content').innerHTML = content;
};

document.getElementById('btnSubmit').addEventListener('click', () => {
  let result = questionList.reduce((sum,currentQuestion,index) => {
    return sum += currentQuestion.checkExact();
  }, 0)
  // for(let question of questionList){
  //   if(question.checkExact()){
  //     result += 1;
  //   };
  // }
  alert(result);
});

document.getElementById('btnFilter1').addEventListener('click', () =>{
  var multipleChoices = questionList.filter((currentQuestion, index)=>{
    return currentQuestion.questionType === 1;
  });
  console.log(multipleChoices);
})
getData();
