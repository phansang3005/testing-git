import Question from "./question.js";

class FillInBlank extends Question {
  // rest parameters thứ nhất sử dụng khi có một hàm chưa truyền tham số và gom tham số truyền vào thành một mảng
  /**
   * function TinhTong(..args){
   *  var sum =0;
   * for(let num of args){
   *      sum += num
   * }
   * console.log(sum)
   * }
   * TinhTong(1,2,3,4,5)
   *
   * sẽ ra là 15
   *
   */

  // rest parameters thứ hai sử dụng khi truyền vào một mảng và muốn lấy ra từng hần tử của mảng
  constructor(...args) {
    // Tuyền dữ liệu đến question
    // spread operator bung từng phần tử trong mảng ra
    super(...args); // kế thừa toạn bộ dữ liệu question
  }
  // Tạo phương thức xuất ra giao diện
  render(index) {
    // string template
    return `
            <div>
                <h4>Câu hỏi ${index} : ${this.content}</h4>
                <input type="text" id="${this._id}"/>
            </div>
        `;
  }
  checkExact() {
    const answer = document.getElementsByClassName("question-" + this._id);
    return answer === this.answers[0].content;
  }
}
export default FillInBlank;
