// export định danh , export ra object Question: Question
// export default khi gọi import không cần phải yêu cầu đúng tên object
class Question {
  constructor(questionType = '', _id = '', content = '', answers = '') {
    this.questionType = questionType;
    this._id = _id;
    this.content = content;
    this.answers = answers;
  }
}
export default Question;
// export default 
//  a: Object,
//  b: Object
// }