import Question from "./question.js";

class MultipleChoice extends Question {
  // rest parameters thứ nhất sử dụng khi có một hàm chưa truyền
  //tham số và gom tham số truyền vào thành một mảng
  constructor(...args) {
    // spread operator bung từng phần tử trong mảng ra
    super(...args); // kế thừa toạn bộ dữ liệu question
  }
  renderAnswer() {
    let answerText = "";
    // tạo vòng lặp trong mảng answer
    for (let ans of this.answers) {
      //let phạm vi sử dụng chỉ nằm trong ngoặc nhọn và sẽ in ra từ 0 đến 4 nếu i < 5, còn var thì in ra năm con số 5
      answerText += `
                <div>
                    <input type="radio" class="question-${this._id}" value="${
        ans._id
      }" name="${this._id}" />
                    <span>${ans.content}</span>
                </div>
            `;
    }
    return answerText;
  }
  render(index) {
    // string template
    return `
            <div>
                <h4>Câu hỏi ${index} : ${this.content}</h4>
                ${this.renderAnswer()}
            </div>
        `;
  }
  checkExact() {
    let exact = false;
    console.log(document.getElementsByClassName("question-" + this._id));
    
    [...document.getElementsByClassName("question-" + this._id)].forEach((ans) => {
        if(ans.checked){
           const ansID = ans.value;
           const checkedAnswer = this.answers.find(ans => { return ans._id === ansID.toString();  
        }) || {};
        return exact = checkedAnswer.exact || false;
           
        //    for(let i in this.answers){
        //        if(this.answers[i]._id === ansID.toString()){
        //            console.log('thuộc tính exact của đáp án :' + this.answers[i].exact);
                   
        //        }
        //    }
        }
       
    });
    return exact;
  }
}
export default MultipleChoice;
/**
 * var a = {hoten: 'hieu'};
 * var b = a;
 * b.hoten = dung thì cả 2 để đổi thành dũng
 * ta có thể sử dụng spred oparator để tách 2 property khác nhau
 * var b = {...a};
 * b.hoten = tai ==> a=hieu, b = tai
 */
